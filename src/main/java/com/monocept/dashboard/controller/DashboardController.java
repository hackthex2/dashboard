package com.monocept.dashboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.monocept.dashboard.request.DashboardService;
import com.monocept.dashboard.request.InputRequest;

@RequestMapping(value = "/api/dashboard/")
public class DashboardController {
	
	private DashboardService dashboardService;
	
	@Autowired
	public DashboardController(DashboardService dashboardService) {
		super();
		this.dashboardService = dashboardService;
	}



	@PostMapping(value = "/getDashboardData")
	public ResponseEntity<Object> getDahsboardData(@RequestBody InputRequest inputRequest)
	{
		return new ResponseEntity<Object>("", HttpStatus.OK);
	}

}
