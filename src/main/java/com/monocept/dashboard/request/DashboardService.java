package com.monocept.dashboard.request;

import com.monocept.dashboard.response.OutputResponse;

public interface DashboardService {
	
	public OutputResponse getDashboardData(InputRequest inputRequest); 

}
